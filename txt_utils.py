
def Right(s, val) :
    amount = len(str(s))
    return "{}{}".format(s, val)[-amount:]

def LeftT(s, val):
    # return s[:amount]
    amount = len(str(s))
    return "{}{}".format(s, val)[amount:]

def Left(s, amount):
    return s[:amount]

def Percent(value, total) :
    if total == 0 :
        return 0
    return round(value * 100 / total, 2)

# def Left(s, val):
#     amount = len(str(val))
#     print( str(val)[:amount] )
#     return "{}{}".format(val, s)[:amount]


def HRTime(time_begin, shortest = True, in_array = False) :
    """ Return Human readable time (hh:mm:ss.ms) """
    l_min = "00"
    l_sec = "00"
    if ":" in str(time_begin) :
        return time_begin
    else :
        # Will return 00:00 if number < 0
        return_time = "00:00"
        if float(time_begin) > 0 :
            l_hour = int(float(time_begin) / 3600)
            l_min = int(float(time_begin) / 60) % 60
            l_sec = (float(time_begin) % 60)
            l_ms = str(int((time_begin - int(time_begin)) * 1000))

            if in_array :
                return [ Right("00", l_hour), Right("00", l_min), Right("00", int(l_sec)), l_ms ]
            return_time = Right("00", l_hour) + ":" + Right("00", l_min) + ":" + Right("00", int(l_sec))
            if shortest :
                if l_hour == 0 :
                    return_time = Right("00", l_min) + ":" + Right("00", int(l_sec))
            else :
                return_time += "." + l_ms
        elif in_array :
            return_time = [0, 0, 0, 0]

        return return_time

def FillValue(value, multiple = 1, add = 0, fill = " ") :
    """
    Return string with the same length of the value
    multiple will multiply value length
    add will add characters to the string
    """
    adj_0 = ""
    for i in range(0, len(str(value)) * multiple + add) :
        adj_0 += fill
    return adj_0

# t1 = 1.51
# t = Left(str(t1) + "00000", 5)
# print(t)
# t = LeftT("00000", t1)
# print(t)
# print(Right("00000", str(t1)))
