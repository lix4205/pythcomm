import sys
import getopt
# TODO : Implement colored output with futil


class usage() :
    """ Helper to manage command line argument and display them in a "beautiful" way ! """

    " Short option : long option "
    OPTIONS_ALIAS = {
        # "n" : "nohelp"
        }
    " OPTIONS_VALUES & OPTION_LOCAL are content of OPTIONS_HELP "
    " 'valued' option "
    OPTIONS_VALUES = {}
    # " No value option "
    # OPTION_LOCAL = []
    " Option : help text "
    OPTIONS_HELP = {
        # "n" : "No help specified !"
        }
    " Mandatory options (avoid [] display in help) "
    OPTIONS_MANDATORY = []
    " Mandatory options (avoid [] display in help) "
    OPTIONS_DEFAULT = {}

    " Args (avoid [] display in help) "
    OPTIONS_ARGS = []
    TEXT = ""
    DESC = ""

    def GetOpts() :
        """
        Return args under getopt.getopt form as the command below "NhH:u:p:P:S:"
        # opts, args = getopt.getopt(argv, "NhH:u:p:P:S:", ["help", "no-synchro", "host=", "user=", ])
        """
        str2return = ":"
        for option in usage.OPTIONS_HELP :
            if option in usage.OPTIONS_ARGS :
                continue
            if option in usage.OPTIONS_ALIAS :
                str2return += option
                if option in usage.OPTIONS_VALUES :
                    str2return += ":"
            else :
                if len(option) == 1 :
                    str2return += option
                    if option in usage.OPTIONS_VALUES :
                        str2return += ":"
        # print(str2return)
        return str2return

    def GetOptsValue() :
        """
        Return args under getopt.getopt form as the command below ["help", "no-synchro", "host=", "user=", ]
        # opts, args = getopt.getopt(argv, "NhH:u:p:P:S:", ["help", "no-synchro", "host=", "user=", ])
        """
        str2return = ""
        array2return = []
        for option in usage.OPTIONS_HELP :
            if option in usage.OPTIONS_ARGS :
                continue
            if option in usage.OPTIONS_ALIAS :
                str2return = usage.OPTIONS_ALIAS[option]
            else :
                str2return = option
            # else :
            #     continue
            if option in usage.OPTIONS_VALUES :
                str2return += "="

            array2return = [*array2return, str2return]

        return array2return

    def print_usage(the_option, the_value = "", the_option_help = "") :
        """ Print usage line for an option """
        if the_option == "" :
            print('{0:>8}{1:42} {2:<0}'.format(the_option, the_value, the_option_help))
        else :
            print('{0:>10}{1:40} {2:<0}'.format(the_option, the_value, the_option_help))

    def usage(options = []) :
        """ Print help """
        print("")
        print("usage: " + usage.TEXT, end=" ")
        idx = 0
        if len(usage.OPTIONS_HELP) > 0 :
            if len(usage.OPTIONS_HELP) > 10 :
                print("[options]", end=" ")
            else :
                for option in usage.OPTIONS_HELP :
                    idx += 1
                    the_alias = ""
                    the_option = "[{}]"
                    if option in usage.OPTIONS_MANDATORY :
                        the_option = "{}"

                    if idx % 4 == 0 :
                        the_option += "\n"
                    if option in usage.OPTIONS_ALIAS :
                        the_alias = "-" + option +",--" + usage.OPTIONS_ALIAS[option]
                    elif option not in usage.OPTIONS_ARGS :
                        # print(len(option))
                        if len(option) == 1 :
                            the_alias = "-" + option
                        else :
                            the_alias = "--" + option
                    else :
                        continue

                    if option in usage.OPTIONS_VALUES :
                        the_alias += " " + usage.OPTIONS_VALUES[option]
                    the_option = the_option.format(the_alias)
                    print(the_option, end=" ")
        # TODO
        # for dist_debian in options :
        #     help.print_usage("", "--" + dist_debian ,  "Install debian " + dist_debian + ".")
        # for option in help.OPTION_LOCAL :
            # print("[--" + option + "]", end=" ")
                print("[--help]", end=" ")
        if len(usage.OPTIONS_ARGS) > 0 :
            for option in usage.OPTIONS_ARGS :
                the_option = "[{}]"
                if option in usage.OPTIONS_MANDATORY :
                    the_option = "{}"
                the_option = the_option.format(option)
                print(the_option, end=" ")

        print("")
        print("Options:")
        if len(usage.OPTIONS_HELP) > 0 :
            for option in usage.OPTIONS_HELP :
                the_alias = ""
                the_option = "-" + option

                if option in usage.OPTIONS_ALIAS :
                    the_alias += ", --" + usage.OPTIONS_ALIAS[option]
                elif option not in usage.OPTIONS_ARGS :
                    the_option = ""
                    if len(option) == 1 :
                        the_alias = "-" + option
                    else :
                        the_alias = "--" + option
                    # the_alias += "--" + option
                else :
                    continue

                if option in usage.OPTIONS_VALUES :
                    the_alias += " [" + usage.OPTIONS_VALUES[option] + "]"

                the_option_help = usage.OPTIONS_HELP[option]
                usage.print_usage(the_option, the_alias, the_option_help)

        usage.print_usage("", "--help",  "Print this help message.")
        if len(usage.OPTIONS_ARGS) > 0 :

            print("Arguments:")
            for option in usage.OPTIONS_ARGS :
                the_alias = ""
                the_option = "[{}]"
                decalage = "{0:>14}"
                decalage_help = "{0:>55}"
                if option in usage.OPTIONS_MANDATORY :
                    the_option = "{}"
                    decalage = "{0:>12}"
                    decalage_help = "{0:>57}"

                the_option = the_option.format(option)
                the_option_help = usage.OPTIONS_HELP[option]
                usage.print_usage("", the_option, the_option_help)

        print("")
        print(usage.DESC)

    def SetDico(argv) :
        dico_args = {}
        # print(usage.OPTIONS_HELP.values())
        # print(usage.OPTIONS_VALUES)

        try:
            opts, args = getopt.getopt(argv, usage.GetOpts(), [*usage.GetOptsValue(), "help"])
        except Exception as ex:
            print(ex)
            usage.usage()
            sys.exit(2)

        for opt in opts :
            if "--help" in opt :
                usage.usage()
                sys.exit(0)
            l_opt = opt[0][1:]
            if l_opt in usage.OPTIONS_HELP.keys() :
                dico_args[l_opt] = ""
            else :
                # if opt.startswith("--") :
                l_opt = opt[0][2:]
                if l_opt in usage.OPTIONS_HELP.keys() :
                    dico_args[l_opt] = ""
                elif l_opt in usage.OPTIONS_ALIAS.values() :
                    for o in usage.OPTIONS_ALIAS :
                        if l_opt == usage.OPTIONS_ALIAS[o] :
                            l_opt = o
                            dico_args[l_opt] = ""
                            break

            if l_opt in usage.OPTIONS_VALUES.keys() :
                dico_args[l_opt] = opt[1]


        if len(args) > 0 :
            idx = 0
            for arg in usage.OPTIONS_ARGS :
                dico_args[arg] = [ args[idx] ]
                idx += 1
                if idx >= len(args) :
                    break
            if len(args) > idx :
                for a in args[idx:] :
                    dico_args[arg].append(a)

        missing_opt = []
        # Check if mandatory options & args are ok
        if len(usage.OPTIONS_MANDATORY) > 0 :
            for opt in usage.OPTIONS_MANDATORY:
                if opt not in dico_args :
                    if opt in usage.OPTIONS_ALIAS :
                        missing_opt.append("Option : --{} is mandatory !".format(usage.OPTIONS_ALIAS[opt]))
                    elif opt in usage.OPTIONS_ARGS :
                        missing_opt.append("Arg : {} is mandatory !".format(opt))
                    else :
                        missing_opt.append("Option : --{} is mandatory !".format(opt))
        if len(missing_opt) :
            raise Exception("\n".join(missing_opt))

        return dico_args
