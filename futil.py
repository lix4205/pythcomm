# coding: utf-8
import sys
import os
import time
import getpass
from multiprocessing.pool import ThreadPool

class msg_lib:
    #NORMAL = '\033[00m'
    #HEADER = '\033[95m'
    #OKBLUE = '\033[94m'
    INFO = 'INFO'
    WARNING = 'WARNING'
    FAIL = 'FAIL'
    ERROR = 'ERROR'
    #ENDC = '\033[0m'
    #BOLD = '\033[1m'
    #UNDERLINE = '\033[4m'

class bcolors:
    NORMAL = '\033[00m'
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class msg :
    def __init__(self, p_b_color = False, show_mode = False) :
        self.COLORED_PROMPT = p_b_color
        self.SHOW_MODE = ""
        if show_mode :
            self.SHOW_MODE = "# "
        self.LOADING = False
        self.PRINT = True
   
    def out(self, msg_type, msg_txt, *msg_replace) :
        """ return string to display """
        msg_fin = "{}".format(msg_type) + msg_txt.format(*msg_replace)
        return msg_fin
        
    def out_n(self, msg_type, msg_txt, *msg_replace) :
        """ out function with color """
        argl = list()
        i = 0
        #type_msg = msg_type
        color_type_msg = "" 
        fin = "" 
        # Default text Color
        text_msg = bcolors.BOLD
        #txt_2replace="" 
        txt = "" 
        if bool(self.COLORED_PROMPT) :
            var_msg = "\033[01;37m" 
            fin_color = "\033[00m"
        else :
            var_msg = "" 
            fin_color = ""
        if msg_replace[0] == "\n" :
            fin += "\n" 
            i += 1
        # print("MSG = ",  msg_type, msg_txt, *msg_replace)
        while i < len(msg_replace) :
            arg = msg_replace[i]
            if bool(self.COLORED_PROMPT) and arg.isdigit() :
                # print("col2 :'"  + str(self.COLORED_PROMPT) + "' " + var_msg + " :: " + fin_color)
                if i + 1 < len(msg_replace) :
                    color_type_msg = "\033[1;" + arg + "m"
                    #print(arg + msg_replace[0] + " " + str(argl[i + 1].isdigit()))
                    if msg_replace[i + 1].isdigit() :
                        var_msg = "\033[1;" + msg_replace[i + 1] + "m"
                    # i += 1
                i += 1
            else :
                if not arg.isdigit() :
                    argl.append(var_msg + arg + fin_color)
            i += 1
        # print(msg_txt.format(*argl))
        if len(argl) > 0 : 
            #print("sa")
            if bool(self.COLORED_PROMPT) :
                txt = text_msg + (msg_txt.format(*argl)) + fin_color
            else :
                txt = msg_txt.format(*argl)
        else :
            txt = msg_txt #.format("")
        if msg_type != "" :
            msg_type = color_type_msg + msg_type + fin_color

        return "{}".format(msg_type) + txt + fin

    # use error/caution function with out_n
    def info(self, msg, *args) :
        print(self.msg_info(msg, *args), end = " ")
    def success(self, msg, *args) :
        print(self.msg_success(msg, *args))
    def error(self, msg, *args) :
        print(self.msg_error(msg, *args))
    def caution(self, msg, *args) :
        print(self.msg_caution(msg, *args))

    def die(self, msg, *args) :
        # TODO : Raise Exception
        self.error(msg, *args)
        sys.exit(1)
        # raise Exception (self.msg_error(msg, *args))

    def msg_info(self, msg, *args) :
        return self.out_n(self.SHOW_MODE + "  -> ", msg, "34", "34", *args)
    def msg_success(self, msg, *args) :
        return self.out_n(self.SHOW_MODE + "==> ", msg, "32", "32", *args)
    def msg_error(self, msg, *args) :
        return self.out_n(self.SHOW_MODE + "==> " + msg_lib.ERROR + ":", msg, "31", "31", *args)
    def msg_caution(self, msg, *args) :
        return self.out_n(self.SHOW_MODE + "==> " + msg_lib.WARNING + ":", msg, "31", "31", *args)
        
    # Original msg, msg2 & die functions from pacstrap
    def msg(self, msg, *args) :
        return self.out(self.SHOW_MODE + "==> " , msg, *args)

    def msg2(self, msg, *args) :
        return self.out(self.SHOW_MODE + "  -> ", msg, *args)

    ## Like msg & msg2 but with color supports
    def msg_n(self, msg, *args) :
        print(self.out_n("==> ", msg, "33", "33" , *args))

    def msg_n2(self, msg, *args) :
        print(self.out_n("  -> " , msg, "33", "33", *args))

    # Like msg_n but with a \n before ==>
    def msg_ntin(self, msg, *args) :
        print(self.out_n("\n==> ", msg, "33", "33" , *args))
    #}>&2
    ## Like msg_n & msg_n2 but without \n
    # TODO : Only one ref, in rid_menu, do not use it !
    def msg_nn(self, msg, *args) :
        saut = "" 
        i = 0
        if len(args) > 0 and args[0].startswith("\\") :
            saut = args[0]
            i += 1
        #if self.PRINT :
        print(self.out_n(saut + "==> ", msg, "33", "33", *args[i:]), end=" ")
        #else :

    def msg_nn2(self, msg, *args) :
        saut = ""
        i = 0
        if len(args) > 0 and (args[0] == "\n" or args[0] == "\r") :
            saut = args[0]
            i += 1
        print(self.out_n(saut + "  -> ", msg, "33", "33", *args[i:]), end=" ")

class menu :
    def __init__(self, p_o_msg):
        self.m_o_msg = p_o_msg

    def print_menu(self, quit, *msg_replace) :
        """
        Display a classic menu
        eg :  1) opt 1
              2) opt 2
                ...
        """
        _q2quit = "q) to quit "
        
        i=0 
        msg_edit = ""
        
        print(self.print_menu_choix(*msg_replace))
        if quit :
            print("\t" + _q2quit, end="")
            print("\n\t", end="")
            

    def print_menu_col(self, quit, *msg_replace) :
        """
        # Create a column menu
        # eg :  1) opt 1       2) opt 2     3) opt 1       4) opt 2
        #       5) opt 1       6) opt 2 ...
        """
        _q2quit = "q) to quit "
        cols = 4
        #format_line = "{}"
        #maxwidth = max(map(lambda x: len(x), msg_replace))
        #justifyList = map(lambda x: x.ljust(maxwidth), msg_replace)
        #lines = (' '.join(justifyList[i:i+cols]) 
                #for i in xrange(0,len(justifyList),cols))
        #print("\n".join(lines))
        #1) af_Afghani
        i=0 
        msg_edit = []
        msg_edit_display = "\n"
        for m2a in msg_replace[0:] :
            i += 1
            msg_edit = [ *msg_edit,  "{0:>4}) {1:<26}".format(i, m2a) ]
            if i % 4 == 0 :
                msg_edit_display += "{0:<20}{1:<20}{2:<20}{3:<20}".format(*msg_edit) + "\n"
                msg_edit = []
        if msg_edit != [] :
            msg_edit_display += self.print_column_menu(len(msg_edit)).format(*msg_edit) + "\n"
        # print(self.msg_nn(msg_replace[0], ""))
        print(msg_edit_display)
        #print(self.print_menu(*msg_replace[1:]), end="")
        if quit :
            print("\n\t" + _q2quit, end="")
            print("\n\t", end="")
    
    def print_column_menu(self, len_col = 4, width_col = 5) :
        """ return a line of menu. """
        i = 0
        str2return = ""
        while i < len_col :
            str2return += '{' + str(i) +':<'+ str(width_col) +'}'
            i += 1

        return str2return

    def print_menu_choix(self, *choices) :
        """ return a line of menu. """
        i = 0 
        msg_edit = ""
        for m2a in choices :
            i += 1
            msg_edit += "\n\t"+ str(i) +") "+ str(m2a)
            
        return msg_edit

# BEGIN "rids" function write a message and wait for an answer from the user
class rid(msg, menu) :
    def __init__(self, p_b_color = False, show_mode = False):
        self.COLORED_PROMPT = p_b_color
        self.PRINT = True
        # print(self.COLORED_PROMPT)
        super().__init__(p_b_color, show_mode)
        
    def ask(self, *msg_replace) :
        """ Ask user for a string """
        tab="  ->" 
        i = 0
        if msg_replace[0].startswith("\\") :
            tab = msg_replace[0]
            i += 1
        val = input(self.out_n( "\r" + tab, msg_replace[i] + " ", "33", "33", *msg_replace[i+1:]))
        return val

    def ask4pass(self, *msg_replace) :
        """ Ask user for a password """
        tab="  ->"
        i = 0
        if msg_replace[0].startswith("\\") :
            tab = msg_replace[0]
            i += 1
        val = getpass.getpass(self.out_n( "\r" + tab, msg_replace[i] + " ", "33", "33", *msg_replace[i+1:]))
        return val

    def ask1char(self, *msg_replace) :
        """ Wait for only one character """
        if len(msg_replace) > 0 :
            tab="  ->"
            self.clear_line()
            print(self.out_n( "\r" + tab, msg_replace[0] + " ", "33", "33", *msg_replace[1:]), end = " ")
        retour = getChar()
        return retour


    def rid_yes_no (self, *msg_replace) :
        """
        rid_yes_no ask yes or no to user and send the answer
        Provide a default value functionality which allow user to type enter for default option
        """
        yes = "yes"
        no = "no"
        
        i = 0
        saut = "\n" 
        exit = False
        txt = ""
        default = "[" + yes + "/" + no.capitalize() + "]"
        if msg_replace[0] == "exit" :
            default = "[" + yes.capitalize() + "/" + no + "]"
            exit = True
            i += 1
        
        retour = self.ask1char( msg_replace[i] + " " + default, *msg_replace[i+1:] ) #.decode()
        #print("GIven :" + retour + " n:" + no[0] + " y:" + yes[0])
        if retour == "\n" :
            saut = ""
        if exit :
            if retour == no[0] :
                print()
                return False
            elif retour == yes[0] or retour == "\n" :
                print()
                return True
        else :
            if retour == no[0] or retour == "\n" :
                print()
                return False
            elif retour == yes[0] :
                print()
                return True
        return self.rid_yes_no( *msg_replace )

    def rid_exit (self, *msg_replace) :
        """ alias for rid_yes_no to exit if """
        if (self.rid_yes_no("exit", *msg_replace) ) :
            return True 
        else :
            sys.exit(2);
    
    def rid_continue (self, *msg_replace) :
        """ alias for read_yes_no """
        return self.rid_yes_no("exit", *msg_replace )

    def clear_line(self) :
        """ force clear line """
        if self.COLORED_PROMPT :
            sys.stdout.write('\033[2K\033[1G')
        else :
            print("\r", end = "")

    def rid_menu(self, *p_o_menu) :
        """
        Print a menu and wait for answer from user
        No bug since a long time... (20231016)
        """
        i = 1
        quit = False
        zero = False
        abort = False
        zero_msg = ""
        default = ""
        column = False
        multiple = False
        menu = []
        if type(p_o_menu[1]) is dict :
            menu = p_o_menu[i+1:]
            # TODO ! 0 Is replacement text
            if "0" in p_o_menu[1].keys() :
                zero = True
                zero_msg = p_o_menu[1]["0"]
            if "abort" in p_o_menu[1].keys() :
                abort = True
            if "quit" in p_o_menu[1].keys() or "exit" in p_o_menu[1].keys() or "q" in p_o_menu[1].keys():
                quit = True
            if "default" in p_o_menu[1].keys() :
                default = p_o_menu[1]["default"] + 1
            if "column" in p_o_menu[1].keys() :
                column = True
            if "multiple" in p_o_menu[1].keys() :
                multiple = True

            # TODO !!!! CASSE GUEULE !!!!
            if "other" in p_o_menu[1].keys() :
                menu = [ *p_o_menu[1]["other"], *menu ]
        else :
            menu = p_o_menu[1:]
            i += 0

        self.msg_nn(str(p_o_menu[0]) + " {}", str(zero_msg))
        if not column :
            self.print_menu(quit, *menu)
        else :
            self.print_menu_col(quit, *menu)
    
        yo = "-1"
        
        while True :
            try :
                if multiple :
                    return_list = []
                    for chx in yo.strip().split(" ") :
                        if chx.isdigit and int(chx) <= len(p_o_menu[i:]) and int(chx) > 0 :
                            return_list.append(int(chx))
                        # else :
                        #     print(str(chx) + " " )
                    if len(return_list) == len(yo.strip().split(" ")) :
                        return return_list
                        # break
                if zero and int(yo) == 0 :
                    return 0
                if yo.isdigit and int(yo) <= len(p_o_menu[i:]) and int(yo) > 0 :
                    break
            except ValueError:
                if yo == "" and default != "" :
                    return default
                if quit and yo == "q" :
                    sys.exit(0)
                if abort and yo == "a" :
                    return -1

            yo = self.ask("Your choice :")
            
        return int(yo)
# END rid_1

# BEGIN Getch

def getChar():
    """ Return input() method for unix or ms station """
    # figure out which function to use once, and store it in _func
    if "_func" not in getChar.__dict__:
        try:
            # for Windows-based systems
            import msvcrt # If successful, we are on Windows
            getChar._func=msvcrt.getch

        except ImportError:
            # for POSIX-based systems (with termios & tty support)
            import tty, sys, termios # raises ImportError if unsupported

            def _ttyRead():
                fd = sys.stdin.fileno()
                oldSettings = termios.tcgetattr(fd)

                try:
                    tty.setcbreak(fd)
                    answer = sys.stdin.read(1)
                    # if answer.startswith("[") :
                    #     tmp = sys.stdin.read(1)
                    #     # while tmp != "" :
                    #     #     tmp += sys.stdin.read(1)
                    #     answer += tmp
                        # print("ANSWER : " + answer)
                finally:
                    termios.tcsetattr(fd, termios.TCSADRAIN, oldSettings)

                return answer

            getChar._func=_ttyRead

    return getChar._func()
# END

"""
NOT USED ?!!
"""
def get_term_size(fd=None):
	"""
	Get the number of lines and columns of the tty that is connected to
	fd.  Returns a tuple of (lines, columns) or (0, 0) if an error
	occurs. The curses module is used if available, otherwise the output of
	`stty size` is parsed. The lines and columns values are guaranteed to be
	greater than or equal to zero, since a negative COLUMNS variable is
	known to prevent some commands from working (see bug #394091).
	"""
	if fd is None:
		fd = sys.stdout
	if not hasattr(fd, 'isatty') or not fd.isatty():
		return (0, 0)
	try:
		import curses
		try:
			curses.setupterm(term=os.environ.get("TERM", "unknown"),
				fd=fd.fileno())
			return curses.tigetnum('lines'), curses.tigetnum('cols')
		except curses.error:
			pass
	except ImportError:
		pass

	try:
		proc = subprocess.Popen(["stty", "size"],
			stdout=subprocess.PIPE, stderr=fd)
	except EnvironmentError as e:
		if e.errno != errno.ENOENT:
			raise
		# stty command not found
		return (0, 0)

	out = _unicode_decode(proc.communicate()[0])
	if proc.wait() == os.EX_OK:
		out = out.split()
		if len(out) == 2:
			try:
				val = (int(out[0]), int(out[1]))
			except ValueError:
				pass
			else:
				if val[0] >= 0 and val[1] >= 0:
					return val
	return (0, 0)



#sys.version
#print("")
#txt="For only {} to {} dollars! {}"
#print(f"{bcolors.FAIL}" + txt.format(pricce = 11) + f"{bcolors.ENDC}" + "lmjk\rtest")
#msg.out("==>", txt, "TESsssT", "ml", "\n", "salut")
#message = msg()
#message.COLORED_PROMPT = True
##message.out_n("==>", txt,"\n", "33", "32", "lkj", "ékjhé l", " SALUT ")
#message.msg(txt, "lkj", "ékjhé l", " SALUT ")
#message.msg2(txt, "lkj", "ékjhé l", " SALUT ")
#message.out_n("==>", txt,"\n", "33", "32", "lkj", "ékjhé l", " SALUT ")
#message.error(txt, "lkj", "ékjhé l", " SALUT ")
#message.caution(txt, "lkj", "ékjhé l", " SALUT ")
#message.msg_n(txt, "lkj", "ékjhé l", " SALUT ")
#message.msg_n2(txt, "lkj", "ékjhé l", " SALUT ")
##message.msg_nn("Veuillez indiquer votre {}", "choix")
#yy = rid(message)
##yo = yy.rid_yes_no( "Veuillez indiquer votre {}", "choix")
##yo = yy.ask("Veuillez indiquer votre {}", "choix")
###yo = yy.ask1char("Veuillez indiquer votre {}", "choix")
###print("te")
##while yo != "o" :
    ##yo = yy.ask1char("Veuillez indiquer votre {}", "choix")
##print(get_term_size())
#mm = menu(yy)
#mm.rid_menu("q", "Choisir", "1", "2", "54")

#yo = yy.ask1char("{}", "Continuer ?")

#while not yo in [ "1", "2", "3" ] :
    #yo = yy.ask1char("{}", "Continuer ?")

##message.die("Hole {}", yo)
#message.msg_ntin(txt, "lkj", "ékjhé l", " SALUT ")
